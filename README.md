Projektas sukurtas Amidus įmonei, kandidatuojant į Junior .Net programuotojo pozicija.

Uždavinio aprašymas:

Reikia sukurti online filmų duomenų bazę ir jos administravimo įrankį. Turi būti realizuotas šis funkcionalumas:

1. Filmo aprašo įvedimas. Įvedami laukai - pavadinimas, išleidimo data, žanras (drama, komedija, veiksmo), bei aktoriai;
2. Filmo aprašo koregavimas;
3. Įvestų filmų sąrašas su galimybe atlikti paiešką pagal pavadinimą, datą, žanrą;
4. Aktorių bei žanrų informacijos koregavimas (įvedimas, redagavimas, šalinimas).

Diegimo vadovas:

1. Reikia instaliuoti:
	- Dotnet core: https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.0.3-windows-x64-binaries
	- Node: https://nodejs.org/en/download/
	- MS Sql server https://www.microsoft.com/en-ca/sql-server/sql-server-downloads
	- GIT https://git-scm.com/downloads
2. Klonuotis projektą į savo kompiuterį:
	- Sukurti naują aplanką, kur bus talpinamas projektas.
	- Atsidaryti 'git bash' naujame aplanke
		- Įvesti git clone https://DovydasPetrutis@bitbucket.org/DovydasPetrutis/amidus.git
3. Projekto paruošimas:
	-Atverti konsolės langą aplanke, kuriame yra projektas ir suvesti šias komandas:
		1. npm install
		2. dotnet restore
		3. webpack --config webpack.config.vendor.js
		4. webpack
		5. dotnet ef database update
		6. dotnet watch run 

*** Duomenų bazė nusistato projekto appsettings.json faile. Pagal nutylėjimą nieko keisti nereikia.