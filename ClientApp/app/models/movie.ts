import { Genre } from "./genre";
import { Actor } from "./actor";

export interface Movie {
    id: number; 
    name: string;
    releaseDate: Date;   
    description: string;
    genre: Genre;
    actors: Actor[];   
}
export interface SaveMovie {
    id: number; 
    name: string;
    releaseDate: string;   
    description: string;
    genreId: number;
    actors: number[];   
  }
