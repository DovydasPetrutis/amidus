export interface Actor {
    id: number; 
    firstName: string;
    lastName: string;
    fullName: string;  
    description: string;   
  }
