import { SaveMovie } from '../models/movie';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http'; 
import 'rxjs/add/operator/map';

@Injectable()
export class MovieService {
  private readonly MovieEndpoint = '/api/movies';

  constructor(private http: Http) { }

  createMovie(movie) {
    return this.http.post(this.MovieEndpoint, movie)
      .map(res => res.json());
  }

  getMovie(id) {
    return this.http.get(this.MovieEndpoint + '/' + id)
      .map(res => res.json());
  }
  getMovies() {
    return this.http.get(this.MovieEndpoint)
      .map(res => res.json());
  }
  updateMovie(movie: SaveMovie) {
    return this.http.put(this.MovieEndpoint + '/' + movie.id, movie)
      .map(res => res.json());
  }

  deleteMovie(id) {
    return this.http.delete(this.MovieEndpoint + '/' + id)
      .map(res => res.json());
  }
}