import { Actor } from '../models/actor';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http'; 
import 'rxjs/add/operator/map';

@Injectable()
export class ActorService {
  private readonly ActorsEndpoint = '/api/actors';

  constructor(private http: Http) { }

  createActor(actor) {
    return this.http.post(this.ActorsEndpoint, actor)
      .map(res => res.json());
  }

  getActor(id) {
    return this.http.get(this.ActorsEndpoint + '/' + id)
      .map(res => res.json());
  }
  getActors() {
    return this.http.get(this.ActorsEndpoint)
      .map(res => res.json());
  }
  updateActor(actor: Actor) {
    return this.http.put(this.ActorsEndpoint + '/' + actor.id, actor)
      .map(res => res.json());
  }

  deleteActor(id) {
    return this.http.delete(this.ActorsEndpoint + '/' + id)
      .map(res => res.json());
  }
}