import { Genre } from '../models/genre';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http'; 
import 'rxjs/add/operator/map';

@Injectable()
export class GenreService {
  private readonly genresEndpoint = '/api/genres';

  constructor(private http: Http) { }

  createGenre(genre) {
    return this.http.post(this.genresEndpoint, genre)
      .map(res => res.json());
  }

  getGenre(id) {
    return this.http.get(this.genresEndpoint + '/' + id)
      .map(res => res.json());
  }
  getGenres() {
    return this.http.get(this.genresEndpoint)
      .map(res => res.json());
  }
  updateGenre(genre: Genre) {
    return this.http.put(this.genresEndpoint + '/' + genre.id, genre)
      .map(res => res.json());
  }

  deleteGenre(id) {
    return this.http.delete(this.genresEndpoint + '/' + id)
      .map(res => res.json());
  }
}