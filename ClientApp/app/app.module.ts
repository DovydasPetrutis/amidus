import { BrowserXhr } from '@angular/http';
import { PaginationComponent } from './components/shared/pagination.component';
import * as Raven from 'raven-js'; 
import { FormsModule } from '@angular/forms'; 
import { NgModule, ErrorHandler } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ToastyModule } from 'ng2-toasty';
import { UniversalModule } from 'angular2-universal';
import { ChartModule } from 'angular2-chartjs';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { AppComponent } from './components/app/app.component'
import { AppErrorHandler } from './app.error-handler';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { AUTH_PROVIDERS } from "angular2-jwt";
import { GenreService } from './services/genre.service';
import { ActorService } from './services/actor.service';
import { MovieService } from './services/movie.service';
import { GenreFormComponent } from './components/genre-form/genre-form.component';
import { GenreListComponent } from './components/genre-list/genre-list';
import { ViewGenreComponent } from './components/genre-view/view-genre';
import { ActorFormComponent } from './components/actor-form/actor-form.component';
import { ActorListComponent } from './components/actor-list/actor-list';
import { ViewActorComponent } from './components/actor-view/view-actor';
import { MovieListComponent } from './components/movie-list/movie-list';
import { MovieFormComponent } from './components/movie-form/movie-form.component';
import { ViewMovieComponent } from './components/movie-view/view-movie';
Raven.config('https://d37bba0c459b46e0857e6e2b3aeff09b@sentry.io/155312').install();

@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,

        PaginationComponent,

        GenreFormComponent,
        GenreListComponent,
        ViewGenreComponent,

        ActorFormComponent,
        ActorListComponent,
        ViewActorComponent,
        
        MovieListComponent,
        MovieFormComponent,
        ViewMovieComponent,
    ],
    imports: [
        UniversalModule, 
        FormsModule,
        AngularMultiSelectModule,
        ToastyModule.forRoot(),
        ChartModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'vehicles', pathMatch: 'full' },

            { path: 'genre/new', component: GenreFormComponent },
            { path: 'genre/edit/:id', component: GenreFormComponent },
            { path: 'genre/:id', component: ViewGenreComponent },
            { path: 'genres', component: GenreListComponent },

            { path: 'actor/new', component: ActorFormComponent },
            { path: 'actor/edit/:id', component: ActorFormComponent },
            { path: 'actor/:id', component: ViewActorComponent },
            { path: 'actors', component: ActorListComponent },

            { path: 'movie/new', component: MovieFormComponent },
            { path: 'movie/edit/:id', component: MovieFormComponent },
            { path: 'movie/:id', component: ViewMovieComponent },
            { path: 'movies', component: MovieListComponent },

            { path: 'home', component: HomeComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    providers: [
      { provide: ErrorHandler, useClass: AppErrorHandler },
      AUTH_PROVIDERS,
      GenreService,
      ActorService,
      MovieService,
    ]
})
export class AppModule {
}
