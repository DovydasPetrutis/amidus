import { Actor} from '../../models/actor';
import * as _ from 'underscore'; 
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { ActorService } from '../../services/actor.service';
import { Component, OnInit } from '@angular/core';
import { ToastyService } from "ng2-toasty";
import 'rxjs/add/Observable/forkJoin';

@Component({
    selector: 'app-actor-form',
    templateUrl: './actor-form.component.html',
    styleUrls: ['./actor-form.component.css']
  })

  export class ActorFormComponent implements OnInit {
       
      actor: Actor = {
      id: 0,
      firstName: '',
      lastName: '',
      description:'',
      fullName: ''
    };
    
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private actorService: ActorService,
        private toastyService: ToastyService) {
    
          route.params.subscribe(p => {
            this.actor.id = +p['id'] || 0;
          });
        }

    ngOnInit()  {   
         if (this.actor.id) {
             this.actorService.getActor(this.actor.id).subscribe(p=>{this.actor = p;});           
        }
        err => {
          if (err.status == 404)
            this.router.navigate(['/actors']);
        };
    }
    private getActor(p: Actor) {
        this.actor.id = p.id;
        this.actor.firstName = p.firstName;
        this.actor.lastName = p.lastName;
        this.actor.description = p.description;      
      } 

      submit() {
        var result$ = (this.actor.id) ? this.actorService.updateActor(this.actor) : this.actorService.createActor(this.actor); 
        result$.subscribe(actor => {
          this.toastyService.success({
            title: 'Success', 
            msg: 'Actor was sucessfully saved.',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
          });
          this.router.navigate(['/actors'])
        });
      }
      
      delete() {
        if (confirm("Are you sure?")) {
          this.actorService.deleteActor(this.actor.id)
            .subscribe(x => {
              this.router.navigate(['/actors']);
            });
        }
      }
}