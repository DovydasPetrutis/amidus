import { Actor } from '../../models/actor';
import { ActorService } from '../../services/actor.service';
import { Component, OnInit } from '@angular/core';


@Component({
    templateUrl: 'actor-list.html'
  })

  export class ActorListComponent implements OnInit {

    actors: Actor[];

    constructor(private actorService: ActorService) { }

    ngOnInit() {
        this.actorService.getActors().subscribe(actors => this.actors = actors)
    }
  }