import { GenreService } from './../../services/genre.service';
import { Movie } from './../../models/movie';
import { MovieService } from './../../services/movie.service';
import { Component, OnInit } from '@angular/core';
import { Genre } from '../../models/genre';
import { ChangeDetectorRef } from '@angular/core';

@Component({
    templateUrl: 'movie-list.html'
  })

  export class MovieListComponent implements OnInit {
    allMovies: Movie[];
    movies = [];
    genres: Genre[];
    filter: any = {};
    curPage : number;
    pageSize : number;
    pages: number;

    constructor(private movieService: MovieService,private genreService: GenreService,
                private cd: ChangeDetectorRef){
                  this.curPage = 1;
                  this.pageSize = 10; }

    ngOnInit() {
        this.movieService.getMovies().subscribe(movies => this.movies = this.allMovies = movies)
        this.genreService.getGenres().subscribe(genres => this.genres = genres);

        this.cd.markForCheck();
    }

    onFilterChange(){
      var movie = this.allMovies;
      console.log(this.filter.name);
       if (this.filter.name)
          movie = movie.filter(p=> p.genre.name.includes(this.filter.name) || p.name.includes(this.filter.name)|| p.releaseDate.toString().includes(this.filter.name));
          this.cd.markForCheck();
      this.movies = movie;
      this.curPage = 1;
      this.pages = Math.ceil(movie.length / this.pageSize);
    }
  onFilterClear(){
    this.movies = this.allMovies;
}
numberOfPages(){
    this.pages = Math.ceil(this.movies.length / this.pageSize);
    return this.pages;
};
}