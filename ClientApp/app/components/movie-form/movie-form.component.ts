import { ActorService } from './../../services/actor.service';
import { GenreService } from './../../services/genre.service';
import { Movie, SaveMovie} from './../../models/movie';
import * as _ from 'underscore'; 
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieService } from './../../services/movie.service';
import { Component, OnInit } from '@angular/core';
import { ToastyService } from "ng2-toasty";
import 'rxjs/add/Observable/forkJoin';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-movie-form',
    templateUrl: './movie-form.component.html',
    styleUrls: ['./movie-form.component.css']
  })

  export class MovieFormComponent implements OnInit {
      datePipe = new DatePipe("lt-LT");
      actors: any[]; 
      genres: any[];
      actorsSelect = [];
      actorsSelected = [];
      dropdownSettings = {};
      movie: SaveMovie = {
      id: 0,
      name: '',
      releaseDate: null,
      genreId:0,
      description:'',
      actors:[] 
    };
    
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private movieService: MovieService,
        private genreService:  GenreService,
        private actorService:  ActorService,
        private toastyService: ToastyService) {
    
          route.params.subscribe(p => {
            this.movie.id = +p['id'] || 0;
          });
        }

    ngOnInit()  {   
      
      var sources = [
        this.genreService.getGenres(),
        this.actorService.getActors(),
        
      ];
      this.dropdownSettings = { 
        singleSelection: false, 
        text:"Select Actors",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        enableSearchFilter: true,
        labelKey:"fullName",
        classes:"myclass custom-class"
      };
      this.actorService.getActors().subscribe(actorsSelect => this.actorsSelect = actorsSelect);
          
      if (this.movie.id)
        sources.push(this.movieService.getMovie(this.movie.id));
  
      Observable.forkJoin(sources).subscribe(data => {
        this.genres = data[0];
        this.actors = data[1];
        if (this.movie.id) {
          this.setMovie(data[2]);
         
        }
      }, err => {
        if (err.status == 404)
          this.router.navigate(['/home']);
      });
      
    }
    private getMovie(p: Movie) {
        this.movie.id = p.id;
        this.movie.name = p.name;
        this.movie.description = p.description;       
      } 
      
      private setMovie(r: Movie) {
        var newDate = this.datePipe.transform(r.releaseDate, 'yyyy-MM-dd');
        this.movie.id = r.id;
        this.movie.name = r.name;
        this.movie.description = r.description;
        this.movie.releaseDate = newDate;
        this.movie.genreId = r.genre.id;
        this.movie.actors=_.pluck(r.actors, 'id');

        
        for(var i = 0; i <this.movie.actors.length;i++){
          var act = this.actors.find(x => x.id == this.movie.actors[i]);
          this.actorsSelected.push({"id":this.movie.actors[i], "fullName":act.fullName});
          console.log(i);
          console.log(this.movie.actors[i]);
        }
        console.log("Selected actor list");
        console.log(this.actorsSelected);
      } 
 
    onItemSelect(actor){
        this.movie.actors.push(actor.id);
        console.log(actor.id);
        //console.log(this.actors);
    }
    OnItemDeSelect(actor){
      var index = this.movie.actors.indexOf(actor.id);
      this.movie.actors.splice(index, 1);
    
    }
    onSelectAll(actor){
      this.movie.actors = [];
      for(var i = 0; i <this.actors.length;i++){
        this.movie.actors.push(this.actors[i].id);
      }
        console.log(actor);
    }
    onDeSelectAll(actor){
      this.movie.actors = [];
        console.log(actor);
    }
      submit() {
        var result$ = (this.movie.id) ? this.movieService.updateMovie(this.movie) : this.movieService.createMovie(this.movie); 
        result$.subscribe(movie => {
          this.toastyService.success({
            title: 'Success', 
            msg: 'Movie was sucessfully saved.',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
          });
          this.router.navigate(['/movies'])
        });
      }
      
      delete() {
        if (confirm("Are you sure?")) {
          this.movieService.deleteMovie(this.movie.id)
            .subscribe(x => {
              this.router.navigate(['/movies']);
            });
        }
      }
}