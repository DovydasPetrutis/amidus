import { Genre } from './../../models/genre';
import { GenreService } from './../../services/genre.service';
import { Component, OnInit } from '@angular/core';


@Component({
    templateUrl: 'genre-list.html'
  })

  export class GenreListComponent implements OnInit {

    genres: Genre[];

    constructor(private genreService: GenreService) { }

    ngOnInit() {
        this.genreService.getGenres().subscribe(genres => this.genres = genres)
    }
  }