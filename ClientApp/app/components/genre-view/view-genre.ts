import { GenreService } from '../../services/genre.service';
import { BrowserXhr } from '@angular/http';
import { ToastyService } from 'ng2-toasty';
import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    templateUrl: 'view-genre.html'
  })
  export class ViewGenreComponent implements OnInit {
    genre: any;
    genreId: number; 
    constructor(
        private route: ActivatedRoute, 
        private router: Router,
        private toasty: ToastyService,
        private genreService: GenreService) { 
    
        route.params.subscribe(p => {
          this.genreId = +p['id'];
          if (isNaN(this.genreId) || this.genreId <= 0) {
            router.navigate(['/genres']);
            return; 
          }
        });
      }

      ngOnInit() {
        this.genreService.getGenre(this.genreId)
        .subscribe(
          v => this.genre = v,
          err => {
            if (err.status == 404) {
              this.router.navigate(['/genres']);
              return; 
            }
          });
    }
    delete() {
        if (confirm("Are you sure?")) {
          this.genreService.deleteGenre(this.genre.id)
            .subscribe(x => {
              this.router.navigate(['/genres']);
            });
        }
      }
}