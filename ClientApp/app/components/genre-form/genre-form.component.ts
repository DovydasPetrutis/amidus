import { Genre} from './../../models/genre';
import * as _ from 'underscore'; 
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { GenreService } from './../../services/genre.service';
import { Component, OnInit } from '@angular/core';
import { ToastyService } from "ng2-toasty";
import 'rxjs/add/Observable/forkJoin';

@Component({
    selector: 'app-genre-form',
    templateUrl: './genre-form.component.html',
    styleUrls: ['./genre-form.component.css']
  })

  export class GenreFormComponent implements OnInit {
       
      genre: Genre = {
      id: 0,
      name: '',
      description:''
    };
    
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private genreService: GenreService,
        private toastyService: ToastyService) {
    
          route.params.subscribe(p => {
            this.genre.id = +p['id'] || 0;
          });
        }

    ngOnInit()  {   
         if (this.genre.id) {
             this.genreService.getGenre(this.genre.id).subscribe(p=>{this.genre = p;});           
        }
        err => {
          if (err.status == 404)
            this.router.navigate(['/genres']);
        };
    }
    private getGenre(p: Genre) {
        this.genre.id = p.id;
        this.genre.name = p.name;
        this.genre.description = p.description;       
      } 

      submit() {
        var result$ = (this.genre.id) ? this.genreService.updateGenre(this.genre) : this.genreService.createGenre(this.genre); 
        result$.subscribe(genre => {
          this.toastyService.success({
            title: 'Success', 
            msg: 'Genre was sucessfully saved.',
            theme: 'bootstrap',
            showClose: true,
            timeout: 5000
          });
          this.router.navigate(['/genres'])
        });
      }
      
      delete() {
        if (confirm("Are you sure?")) {
          this.genreService.deleteGenre(this.genre.id)
            .subscribe(x => {
              this.router.navigate(['/genres']);
            });
        }
      }
}