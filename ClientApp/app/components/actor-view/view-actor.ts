import { ActorService } from '../../services/actor.service';
import { BrowserXhr } from '@angular/http';
import { ToastyService } from 'ng2-toasty';
import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    templateUrl: 'view-actor.html'
  })
  export class ViewActorComponent implements OnInit {
    actor: any;
    actorId: number; 
    constructor(
        private route: ActivatedRoute, 
        private router: Router,
        private toasty: ToastyService,
        private actorService: ActorService) { 
    
        route.params.subscribe(p => {
          this.actorId = +p['id'];
          if (isNaN(this.actorId) || this.actorId <= 0) {
            router.navigate(['/actors']);
            return; 
          }
        });
      }

      ngOnInit() {
        this.actorService.getActor(this.actorId)
        .subscribe(
          v => this.actor = v,
          err => {
            if (err.status == 404) {
              this.router.navigate(['/actors']);
              return; 
            }
          });
    }
    delete() {
        if (confirm("Are you sure?")) {
          this.actorService.deleteActor(this.actor.id)
            .subscribe(x => {
              this.router.navigate(['/actors']);
            });
        }
      }
}