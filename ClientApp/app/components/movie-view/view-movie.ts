import { MovieService } from './../../services/movie.service';
import { BrowserXhr } from '@angular/http';
import { ToastyService } from 'ng2-toasty';
import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    templateUrl: 'view-movie.html'
  })
  export class ViewMovieComponent implements OnInit {
    movie: any;
    movieId: number; 
    constructor(
        private route: ActivatedRoute, 
        private router: Router,
        private toasty: ToastyService,
        private movieService: MovieService) { 
    
        route.params.subscribe(p => {
          this.movieId = +p['id'];
          if (isNaN(this.movieId) || this.movieId <= 0) {
            router.navigate(['/movies']);
            return; 
          }
        });
      }

      ngOnInit() {
        this.movieService.getMovie(this.movieId)
        .subscribe(
          v => this.movie = v,
          err => {
            if (err.status == 404) {
              this.router.navigate(['/movies']);
              return; 
            }
          });
    }
    delete() {
        if (confirm("Are you sure?")) {
          this.movieService.deleteMovie(this.movie.id)
            .subscribe(x => {
              this.router.navigate(['/movies']);
            });
        }
      }
}